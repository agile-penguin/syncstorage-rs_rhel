# About this repo

This repo was created by [Agile-Penguin](https://agile-penguin.com).
It holds instruccions to the syncstorage installation on RHEL but with no warranty.

The instructions are available in:
* Spanish
* German
* English
