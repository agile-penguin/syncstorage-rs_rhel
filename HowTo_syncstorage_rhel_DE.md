# mozilla syncstorage-rs rhel

Instruktion zur Installation von [mozilla syncstorage-rs](https://github.com/mozilla-services/syncstorage-rs) server auf RHEL Systemen, wie beispielsweise Fedora, Centos, RockyLinux, etc. (Ebenfalls verfügbar auf englisch und spanisch).
Dies ist ein privates Repository, das bedeutet, dass es nur Instruktionen enthält. Es ist weder von MOZILLA erstellt noch von MOZILLA gewartet.

Für andere LINUX Distributionen, wie z. B. ArchLinux, kann [dieses repositorio](https://aur.archlinux.org/packages/firefox-syncstorage-git) verwendet werden. Ich habe es selbst auch für Details verwendet.

## Bevor wir starten

Um starten zu können benötigst du, wie bereits erwähnt, ein RHEL System mit Adminrechten bzw. Zugang als root.
Diese Anleitung ist für frisch installierte Systeme erstellt. Falls bereits Software installiert wurde, kann es zu Abweichungen kommen.

Um den Syncstorage zu installieren und zu betreiben wird MariaDB und httpd verwendet. Natürlich gibt es weitere Möglichkeiten, die hier jedoch nicht betrachtet werden.

## Agenda

- [ ] Vorbedingungen
- [ ] Installation des Syncstorages
- [ ] Syncstorage als systemd Service
- [ ] Reverse proxy server / webserver 
- [ ] Firefox Konfiguration


## Vorbedingungen

Starte eine Root-Session, sodass der  ```#```-Prompt ersichtlich ist.
Zu Beginn, aktualisiere dein System:
```
dnf update
```

Folge den Installationsanweisungen:
```
sudo dnf install -y cmake gcc golang libcurl-devel openssl-devel make pkg-config curl httpd
```

| RHEL 8                                        |                           RHEL 9                          |
|-----------------------------------------------|:---------------------------------------------------------:|
| ``` sudo dnf install -y protobuf-compiler ``` | ``` dnf --enablerepo=crb install -y protobuf-compiler ``` |




```
sudo dnf -y install epel-release
sudo dnf -y update
sudo yum -y groupinstall "Development Tools"
sudo dnf install -y python3-devel

```

Nun, installiere Rust:
```
sudo curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```
Bestätige mit ```1``` und < Enter >.
Um später cargo et. nutzen zu können gib Folgendes ein:
```
source $HOME/.cargo/env
```

### Installation der Datenbank

Installiere MariaDB und starte sie.

| RHEL 8                                                                                                                            |                                                                               RHEL 9                                                                              |
|-----------------------------------------------------------------------------------------------------------------------------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------:|
| ``` sudo dnf install -y mariadb mariadb-server mariadb-devel systemctl enable mariadb.service systemctl start mariadb.service ``` | ``` sudo dnf install -y mariadb mariadb-server dnf --enablerepo=crb install -y mariadb-devel systemctl enable mariadb.service systemctl start mariadb.service ``` |


Die initiale Konfiguration wird wie folgt initiiert.
```
mysql_secure_installation
```
Folge den Anweisungen und gib deine präferierten Optionen ein. Falls MariaDB bereits installiert wurde, muss dieser Schritt nicht gemacht werden.

Nun können der user und die Datenbanken angelegt werden.
Logge dich in die DB ein:
```
sudo mysql -u root -p
```
Gib dein Passwort ein und bestätige mit < Enter >.

Vergiss nicht ```username``` und ```password``` zu ändern.
```
CREATE USER "user"@"localhost" IDENTIFIED BY "password";
CREATE DATABASE syncstorage_rs;
GRANT ALL PRIVILEGES on syncstorage_rs.* to user@localhost;

CREATE DATABASE tokenserver_rs;
GRANT ALL PRIVILEGES on tokenserver_rs.* to user@localhost;

quit;
```

## Syncstorage-rs Installation

Zunächst, wird nach ```/opt/``` navigiert und dann das Git Repository geklont.
```
cd /opt/
git clone https://github.com/mozilla-services/syncstorage-rs.git
cd syncstorage-rs/config
cp local.example.toml local.toml
nano local.toml
```
Wir ändern die Konfigurationsdatei, sodass sie unseren Vorstellungen entspricht. Noch einmal, vergiss nicht ```user``` , ```password``` und die secrets zu ändern.
-Hinweis: Falls ein spezieller Port benutzt werden soll, kann dieser hier ebenfalls definiert werden. Hierzu wird in eine Zeile ```port = nummer``` geschrieben. Der Standardport ist 8000.

```
# Example MySQL DSN:
syncstorage.database_url = "mysql://user:password@localhost/syncstorage_rs"

# Example Spanner DSN:
# database_url="spanner://projects/SAMPLE_GCP_PROJECT/instances/SAMPLE_SPANNER_INSTANCE/databases/SAMPLE_SPANNER_DB"

"limits.max_total_records"=1666 # See issues #298/#333
master_secret = "mySecret"

# removing this line will default to moz_json formatted logs (which is preferred for production envs)
human_logs = 1

# enable quota limits
syncstorage.enable_quota = 0
syncstorage.enabled = true
syncstorage.limits.max_total_records = 1666 # See issues #298/#333
# set the quota limit to 2GB.
# max_quota_limit = 200000000

# Example Tokenserver settings:
disable_syncstorage = false
tokenserver.database_url = "mysql://user:password@localhost/tokenserver_rs"
tokenserver.enabled = true

tokenserver.fxa_email_domain = "api-accounts.firefox.com"
tokenserver.fxa_metrics_hash_secret = "mySecret"
tokenserver.fxa_oauth_server_url = "https://oauth.accounts.firefox.com/v1"
tokenserver.test_mode_enabled = false
tokenserver.run_migrations = true

# cors settings
cors_allowed_origin = "null"
cors_max_age = 86400
```

Speichere und schließe die Datei.

### Build

Der gesamte Teil kann direkt gebaut werden mit:
```
cargo build
```
im Anschluss dann
```
make run_mysql
```
Dieser Schritt dauert ein paar Minuten.
Wenn alles gut geht wird am Ende angezeigt ...2 workers started. Terminiere den Prozess mit "ctrl+c".

### DB Inhalt

Das Wichtigste ist den Datenbank Inhalt der "tokenserver-db" nicht zu vergessen. Vergiss hier nicht deine korrekte Domäne mit dem adäquaten Präfix (http o https) einzutragen.
```
sudo mysql -u root -p

USE tokenserver_rs;
INSERT INTO `services` (`id`, `service`, `pattern`) VALUES ('1', 'sync-1.5', '{node}/1.5/{uid}');
INSERT INTO `nodes` (`id`, `service`, `node`, `available`, `current_load`, `capacity`, `downed`, `backoff`) VALUES ('1', '1', 'https://mydomain.tld', '1', '0', '1', '0', '0');

quit;
```

### Syncstorage als systemd Service
#### Startscript
Um den Service als solches zu starten, wird ein Startscript benötigt. Es wird im gleichen Ordner gespeichert:
```
nano /opt/syncstorage-rs/syncstorageStarter.sh
```
Füge die folgenden Zeilen ein und speichere.
```
#!/bin/bash
source /opt/syncstorage-rs/venv/bin/activate
RUST_LOG=debug RUST_BACKTRACE=full /root/.cargo/bin/cargo run -- --config config/local.toml
```
Füge das Execute-Flag hinzu:
```
chmod +x /opt/syncstorage-rs/syncstorageStarter.sh
```

#### Service-Datei
Erstelle die Service-Datei:
```
nano /etc/systemd/system/syncstorage.service
```
Gib Folgendes ein und speichere:
```
[Unit]
Description=Mozilla Syncstorage-RS Server
After=network.target nss-lookup.target httpd-init.service
Wants=mysql.service

[Install]
WantedBy=multi-user.target
Alias=syncstorage.service

[Service]
Type=simple
WorkingDirectory=/opt/syncstorage-rs/
ExecStart=/opt/syncstorage-rs/syncstorageStarter.sh
TimeoutSec=100
TimeoutStartSec=180
Restart=on-abort

KillMode=mixed
KillSignal=SIGTERM
```

Lade die Liste der Services neu und starte den neu angelegten:
```
systemctl daemon-reload
systemctl start syncstorage
```
## Reverse proxy / httpd

Starte und konfiguriere httpd:
```
systemctl start httpd
systemctl enable httpd

sudo firewall-cmd --zone=public --permanent --add-service=http
sudo firewall-cmd --zone=public --permanent --add-service=https
sudo firewall-cmd --reload 
setsebool -P httpd_can_network_connect 1
```
Die SELinux-Konfiguration per "setsebool" erlaubt httpd den Zugriff auf localhost per 127.0.0.1.

Als letzten Schritt muss der Virtualhost entweder für http oder https konfiguriert werden. Für https werden Zertifikate benötigt. Genaueres [hier](https://www.digitalocean.com/community/tutorials/how-to-create-a-self-signed-ssl-certificate-for-apache-on-centos-8)  

```
cd /etc/httpd/conf.d/
nano mozillaSync.conf
```

Für http verwende: 
```
<VirtualHost *:80>
  ServerName localhost
  DocumentRoot /opt/syncstorage-rs/
  ProxyPreserveHost On
  ProxyPass / http://127.0.0.1:8000/
  ProxyPassReverse / http://127.0.0.1:8000/
  <Proxy *>
     AllowOverride all
     Require all granted
  </Proxy>
</VirtualHost>
```

oder für https:
```
<VirtualHost *:443>
   SSLEngine on
   SSLCertificateFile /path/to.crt
   SSLCertificateKeyFile /path/to.key
   ServerName myserver.tld
   DocumentRoot /opt/syncstorage-rs/
   RequestHeader set X-Forwarded-Proto https
   ProxyPreserveHost On
   ProxyPass / http://127.0.0.1:8000/
   ProxyPassReverse / http://127.0.0.1:8000/
   <Proxy *>
      AllowOverride all
      Require all granted
   </Proxy>
</VirtualHost>
```
Starte httpd neu:

```
systemctl restart httpd
```

## Firefox Konfiguration
öffne Firefox und navigiere zu "about:config". In der Suchleiste gib ```tokenserver.uri``` ein. Hier wird Folgendes eingegeben:
```
http://yourip/1.0/sync/1.5
https://domain.tlz/1.0/sync/1.5
```
Speichere und starte Firefox neu.

Jetzt kann eingeloggt oder ein Konto angelegt und schlussendlich synchronisiert werden.
Ahora puedes iniciar sesión o crear una cuenta y, por fin, sincronizar.


## Author y Acknowledgement
Der Author dieser Anleitung ist [agile-penguin](agile-penguin.com).
Aber dies wäre ohne die Unterstützung der Community, allen voran Jewelux, nicht möglich gewesen.

## Lizenz
Wie bereits erwähnt ist dies nicht von MOZILLA und steht daher unter keiner Lizenz. Der Inhalt von MOZILLA seinerseits hat eine Lizenz die sich [hier](https://github.com/mozilla-services/syncstorage-rs/blob/master/LICENSE) befindet.
