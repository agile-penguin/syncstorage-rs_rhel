# mozilla syncstorage-rs rhel

Instrutions to install / setup [mozilla syncstorage-rs](https://github.com/mozilla-services/syncstorage-rs) server on RHEL systems, like fedora, Centos, RockyLinux, etc. (Also available in spanish and german).
This is a private repository which only gives instructions to install, but does not belong to and is not maintained by MOZILLA.

For other LINUX systems, e.g. ArchLinux, you can use [this repository](https://aur.archlinux.org/packages/firefox-syncstorage-git). I also used some parts of it.

## Before we begin

To be able to start, you will need a RHEL based system, as mentioned above, with admin rights / access as root user.
This instruction is meant for plain Linux systems. If you have already installed packages or modules, set up webservers or policies, the behaviour may differ. 

To install and run this server, we will install / use mariadb and httpd. This is, of course, not the only possibility, but the only one used by this instruction.

## Agenda

- [ ] Prerequisites
- [ ] Syncstorage installation
- [ ] Syncstorage as systemd service
- [ ] Reverse proxy server / webserver 
- [ ] Firefox configuration


## Prerequisites

Login as root so you see the ```#``` prompt.
First of all, update your system using
```
dnf update
```

Go on with the installation like this:
```
sudo dnf install -y cmake gcc golang libcurl-devel openssl-devel make pkg-config curl httpd
```

| RHEL 8                                        |                           RHEL 9                          |
|-----------------------------------------------|:---------------------------------------------------------:|
| ``` sudo dnf install -y protobuf-compiler ``` | ``` dnf --enablerepo=crb install -y protobuf-compiler ``` |


```
sudo dnf -y install epel-release
sudo dnf -y update
sudo yum -y groupinstall "Development Tools"
sudo dnf install -y python3-devel

```

Now install rust:
```
sudo curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```
Confirm by pressing ```1``` and < Enter >.
To later be able to use cargo etc. type:
```
source $HOME/.cargo/env
```

### Database installation
Now we install MariaDB server and start the service

| RHEL 8                                                                                                                            |                                                                               RHEL 9                                                                              |
|-----------------------------------------------------------------------------------------------------------------------------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------:|
| ``` sudo dnf install -y mariadb mariadb-server mariadb-devel systemctl enable mariadb.service systemctl start mariadb.service ``` | ``` sudo dnf install -y mariadb mariadb-server dnf --enablerepo=crb install -y mariadb-devel systemctl enable mariadb.service systemctl start mariadb.service ``` |


The initial configuration of mariadb is done by
```
mysql_secure_installation
```
Just follow the menu and insert the option you prefer.

Once installed, we can create the user and the database. 
We login to mariadb using:
```
sudo mysql -u root -p
```
Insert your password and press Enter.
Don't forget to change ```username``` and ```password```.
```
CREATE USER "user"@"localhost" IDENTIFIED BY "password";
CREATE DATABASE syncstorage_rs;
GRANT ALL PRIVILEGES on syncstorage_rs.* to user@localhost;

CREATE DATABASE tokenserver_rs;
GRANT ALL PRIVILEGES on tokenserver_rs.* to user@localhost;

quit;
```

## Syncstorage-rs installation

First of all we navigate to the ```/opt/```directory and then clone the complete git repo.
```
cd /opt/
git clone https://github.com/mozilla-services/syncstorage-rs.git
cd syncstorage-rs/config
cp local.example.toml local.toml
nano local.toml
```
We modify the configuration file to suite our needs. Again, don't forget to modify ```user``` , ```password``` and the secrets.
-Hint: If you want to use a special port, you can define that here as well: Just add a line with ```port = number```. The default port is 8000.

```
# Example MySQL DSN:
syncstorage.database_url = "mysql://user:password@localhost/syncstorage_rs"

# Example Spanner DSN:
# database_url="spanner://projects/SAMPLE_GCP_PROJECT/instances/SAMPLE_SPANNER_INSTANCE/databases/SAMPLE_SPANNER_DB"

"limits.max_total_records"=1666 # See issues #298/#333
master_secret = "mySecret"

# removing this line will default to moz_json formatted logs (which is preferred for production envs)
human_logs = 1

# enable quota limits
syncstorage.enable_quota = 0
syncstorage.enabled = true
syncstorage.limits.max_total_records = 1666 # See issues #298/#333
# set the quota limit to 2GB.
# max_quota_limit = 200000000

# Example Tokenserver settings:
disable_syncstorage = false
tokenserver.database_url = "mysql://user:password@localhost/tokenserver_rs"
tokenserver.enabled = true

tokenserver.fxa_email_domain = "api-accounts.firefox.com"
tokenserver.fxa_metrics_hash_secret = "mySecret"
tokenserver.fxa_oauth_server_url = "https://oauth.accounts.firefox.com/v1"
tokenserver.test_mode_enabled = false
tokenserver.run_migrations = true

# cors settings
cors_allowed_origin = "null"
cors_max_age = 86400
```

Save and exit the config file.

### Build

Before we run the server, we build the openssl-part. Navigate to /opt/syncstorage-rs/ and then type:
```
cargo build
```

After completing this, we can finally run the service:
```
make run_mysql
```
If everything works and you can see the message that tells 2 workers started, terminate the process by sending "ctrl+c".

### DB content

The most important point is to fill the tokenserver-db with the following content. Don't forget to insert your correct domain or ip with http or https.
Furthermore the value for capacity should meet the number of connections / registered accounts to syunc with.
```
sudo mysql -u root -p

USE tokenserver_rs;
INSERT INTO `services` (`id`, `service`, `pattern`) VALUES ('1', 'sync-1.5', '{node}/1.5/{uid}');
INSERT INTO `nodes` (`id`, `service`, `node`, `available`, `current_load`, `capacity`, `downed`, `backoff`) VALUES ('1', '1', 'https://mydomain.tld', '1', '0', '4', '0', '0');

quit;
```

### syncstorage as systemd service
#### starter script
We need a start script that we store in the syncstorage folder:
```
nano /opt/syncstorage-rs/syncstorageStarter.sh
```
Insert the following lines and save it.
```
#!/bin/bash
source /opt/syncstorage-rs/venv/bin/activate
RUST_LOG=debug RUST_BACKTRACE=full /root/.cargo/bin/cargo run -- --config config/local.toml
```
Add the execution flag to the file:
```
chmod +x syncstorageStarter.sh
```

#### service file
Create the service file like
```
nano /etc/systemd/system/syncstorage.service
```
Insert the following lines and save it.
```
[Unit]
Description=Mozilla Syncstorage-RS Server
After=network.target nss-lookup.target httpd-init.service
Wants=mysql.service

[Install]
WantedBy=multi-user.target
Alias=syncstorage.service

[Service]
Type=simple
WorkingDirectory=/opt/syncstorage-rs/
ExecStart=/opt/syncstorage-rs/syncstorageStarter.sh
TimeoutSec=100
TimeoutStartSec=180
Restart=on-abort

KillMode=mixed
KillSignal=SIGTERM
```

Reload the service list and start the service:
```
systemctl daemon-reload
systemctl start syncstorage
```
## Reverse proxy / httpd

To configure the httpd type:
```
systemctl start httpd
systemctl enable httpd

sudo firewall-cmd --zone=public --permanent --add-service=http
sudo firewall-cmd --zone=public --permanent --add-service=https
sudo firewall-cmd --reload 
setsebool -P httpd_can_network_connect 1
```
The setsebool configures SELinux so that httpd can access the service under 127.0.0.1.

The last step is to set the virtualhost for either http or https. Remember that you need a certificate for TLS just in case you want to use https.
```
cd /etc/httpd/conf.d/
nano mozillaSync.conf
```

Now either use this snippet for http
```
<VirtualHost *:80>
  ServerName localhost
  DocumentRoot /opt/syncstorage-rs/
  ProxyPreserveHost On
  ProxyPass / http://127.0.0.1:8000/
  ProxyPassReverse / http://127.0.0.1:8000/
  <Proxy *>
     AllowOverride all
     Require all granted
  </Proxy>
</VirtualHost>
```

or that snippet for https:
```
<VirtualHost *:443>
   SSLEngine on
   SSLCertificateFile /path/to.crt
   SSLCertificateKeyFile /path/to.key
   ServerName myserver.tld
   DocumentRoot /opt/syncstorage-rs/
   RequestHeader set X-Forwarded-Proto https
   ProxyPreserveHost On
   ProxyPass / http://127.0.0.1:8000/
   ProxyPassReverse / http://127.0.0.1:8000/
   <Proxy *>
      AllowOverride all
      Require all granted
   </Proxy>
</VirtualHost>
```
Restart httpd

```
systemctl restart httpd
```

## Firefox configuration
Open firefox browser and navigate to "about:config". In the search bar insert ```tokenserver.uri```. Here you have to give one of the following
```
http://yourip/1.0/sync/1.5
https://domain.tlz/1.0/sync/1.5
```
Save and restart firefox.

Now you can create an account or sign in and then finally sync to your own syncstorage server.


## Authors and acknowledgment
The author of this instruction is [agile-penguin](agile-penguin.com).
But only with the help from the community, esapecially Jewelux, was this possible.

## License
As mentioned above, this is only an instruction on how to install mozilla's syncstorage-rs. That's why there is no special license for this repository. With respect to mozilla's content, please visit [their license agreement](https://github.com/mozilla-services/syncstorage-rs/blob/master/LICENSE).


