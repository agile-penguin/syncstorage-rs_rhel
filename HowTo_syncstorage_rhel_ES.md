# mozilla syncstorage-rs rhel

Instrucciones de cómo instalar [mozilla syncstorage-rs](https://github.com/mozilla-services/syncstorage-rs) server en sistemas RHEL, como fedora, Centos, RockyLinux, etc. (También está disponible en inglés y alemán).
Este es un repositorio privado que a su vez solamente da instrucciones y apoyo pero no es propiedad de MOZILLA ni lo mantiene.

Para otras distribuciones, por ejemplo ArchLinux, puedes visitar [este repositorio](https://aur.archlinux.org/packages/firefox-syncstorage-git). Yo también lo he utilizado para unos detalles.

## Antes de empezar

Para poder empezar, necesitas un sistema RHEL, como se ha mencionado antes, con privilegios de admin, o sea acceso como root.
Este manual es para sistemas blancos. Si ya están instalados servicios o aplicaciones, puede que difiera.

Para instalar y lanzar el syncstorage necesitas instalar o usar MariaDB y httpd. Está claro que no es la única posibilidad, pero es la única utilizada en este manual.

## Agenda

- [ ] Prerrequisitos
- [ ] Instalación de Syncstorage
- [ ] Syncstorage como servicio systemd
- [ ] Reverse proxy server / webserver 
- [ ] Configuración en Firefox


## Prerrequisitos

Inicia sesión como root para poder ver el prompt ```#```.
Para empezar, actualiza tu sistema:
```
dnf update
```

Sigue estas instalaciones:
```
sudo dnf install -y cmake gcc golang libcurl-devel openssl-devel make pkg-config curl httpd
```

| RHEL 8                                        |                           RHEL 9                          |
|-----------------------------------------------|:---------------------------------------------------------:|
| ``` sudo dnf install -y protobuf-compiler ``` | ``` dnf --enablerepo=crb install -y protobuf-compiler ``` |




```
sudo dnf -y install epel-release
sudo dnf -y update
sudo yum -y groupinstall "Development Tools"
sudo dnf install -y python3-devel

```

Ahora, instala rust:
```
sudo curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```
Confirma al hacer clic ```1``` y < Entrar >.
Para que puedas utilizar cargo etc. escribe:
```
source $HOME/.cargo/env
```

### Instalación de la base de datos

Instala la base de datos, MariaDB server, y lánzala.

| RHEL 8                                                                                                                            |                                                                               RHEL 9                                                                              |
|-----------------------------------------------------------------------------------------------------------------------------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------:|
| ``` sudo dnf install -y mariadb mariadb-server mariadb-devel systemctl enable mariadb.service systemctl start mariadb.service ``` | ``` sudo dnf install -y mariadb mariadb-server dnf --enablerepo=crb install -y mariadb-devel systemctl enable mariadb.service systemctl start mariadb.service ``` |


La configuración inicial de MariaDb se usa con
```
mysql_secure_installation
```
Sigue las instrucciones y escoge las opciones preferidas. Si ya estaba instalado MariaDB, no lo tienes que hacer.

Una vez instalado, podemos crear el usuario y las bases de datos.
Inicia sesión con lo siguiente:
```
sudo mysql -u root -p
```
Inserta tu contraseña y haz clic en < Entrar >.
No olvides cambiar ```username``` y ```password```.
```
CREATE USER "user"@"localhost" IDENTIFIED BY "password";
CREATE DATABASE syncstorage_rs;
GRANT ALL PRIVILEGES on syncstorage_rs.* to user@localhost;

CREATE DATABASE tokenserver_rs;
GRANT ALL PRIVILEGES on tokenserver_rs.* to user@localhost;

quit;
```

## Instalación Syncstorage-rs

Primero, navegamos a ```/opt/``` y después clonamos el repositorio git.
```
cd /opt/
git clone https://github.com/mozilla-services/syncstorage-rs.git
cd syncstorage-rs/config
cp local.example.toml local.toml
nano local.toml
```
Modificamos el archivo de configuración para que cumpla nuestras necesidades. Otra vez, no olvides que tienes que cambiar  ```user``` , ```password``` y los secretos (secret).
-Nota: Si quieres o necesitas usar un puerto especifico, también lo puedes configurar aquí. Solo añade una línea en la que pones el puerto como ```port = numero```. El puerto predeterminado es 8000.

```
# Example MySQL DSN:
syncstorage.database_url = "mysql://user:password@localhost/syncstorage_rs"

# Example Spanner DSN:
# database_url="spanner://projects/SAMPLE_GCP_PROJECT/instances/SAMPLE_SPANNER_INSTANCE/databases/SAMPLE_SPANNER_DB"

"limits.max_total_records"=1666 # See issues #298/#333
master_secret = "mySecret"

# removing this line will default to moz_json formatted logs (which is preferred for production envs)
human_logs = 1

# enable quota limits
syncstorage.enable_quota = 0
syncstorage.enabled = true
syncstorage.limits.max_total_records = 1666 # See issues #298/#333
# set the quota limit to 2GB.
# max_quota_limit = 200000000

# Example Tokenserver settings:
disable_syncstorage = false
tokenserver.database_url = "mysql://user:password@localhost/tokenserver_rs"
tokenserver.enabled = true

tokenserver.fxa_email_domain = "api-accounts.firefox.com"
tokenserver.fxa_metrics_hash_secret = "mySecret"
tokenserver.fxa_oauth_server_url = "https://oauth.accounts.firefox.com/v1"
tokenserver.test_mode_enabled = false
tokenserver.run_migrations = true

# cors settings
cors_allowed_origin = "null"
cors_max_age = 86400
```

Guarda el archivo y sal.

### Build

Antes de lanzar el servicio, debemos construir la parte de openssl. Navega a /opt/syncstorage-rs/ e inserta:
```
cargo build
```

Después, por fin, podemos construir y lanzar todo el servicio:
```
make run_mysql
```
Este paso dura unos minutos. 
Si sale bien, al final te dice ...2 workers started. Termina el proceso con "ctrl+c".

### Contenido de la base de datos

Lo más importante es no olvidar insertar el contenido necesario en la base de datos "tokenserver-db". No olvides utilizar tu dominio y el prefijo adecuado (http o https)
```
sudo mysql -u root -p

USE tokenserver_rs;
INSERT INTO `services` (`id`, `service`, `pattern`) VALUES ('1', 'sync-1.5', '{node}/1.5/{uid}');
INSERT INTO `nodes` (`id`, `service`, `node`, `available`, `current_load`, `capacity`, `downed`, `backoff`) VALUES ('1', '1', 'https://mydomain.tld', '1', '0', '1', '0', '0');

quit;
```

### Syncstorage como servicio systemd
#### script de lanzamiento
Para lanzar el servicio como tal, necesitamos un script de lanzamiento. Lo guardamos en la misma carpeta:
```
nano /opt/syncstorage-rs/syncstorageStarter.sh
```
Pega las siguientes líneas y guárdalo.
```
#!/bin/bash
source /opt/syncstorage-rs/venv/bin/activate
RUST_LOG=debug RUST_BACKTRACE=full /root/.cargo/bin/cargo run -- --config config/local.toml
```
Añade la opción de ejecutar al archivo nuevo:
```
chmod +x syncstorageStarter.sh
```

#### archivo de service
Crea el archivo de servicio (service):
```
nano /etc/systemd/system/syncstorage.service
```
Inserta lo siguiente y guárdalo:
```
[Unit]
Description=Mozilla Syncstorage-RS Server
After=network.target nss-lookup.target httpd-init.service
Wants=mysql.service

[Install]
WantedBy=multi-user.target
Alias=syncstorage.service

[Service]
Type=simple
WorkingDirectory=/opt/syncstorage-rs/
ExecStart=/opt/syncstorage-rs/syncstorageStarter.sh
TimeoutSec=100
TimeoutStartSec=180
Restart=on-abort

KillMode=mixed
KillSignal=SIGTERM
```

Recarga la lista de servicios y lanza el nuestro:
```
systemctl daemon-reload
systemctl start syncstorage
```
## Reverse proxy / httpd

Lanza y configura httpd:
```
systemctl start httpd
systemctl enable httpd

sudo firewall-cmd --zone=public --permanent --add-service=http
sudo firewall-cmd --zone=public --permanent --add-service=https
sudo firewall-cmd --reload 
setsebool -P httpd_can_network_connect 1
```
La configuración de SELinux, "setsebool", permite al servicio httpd acceder a localhost usando su dirección 127.0.0.1.

Como último paso, hemos de configurar el virtualhost usando o http o https. Si usas https, necesitas certificados. Si no sabes de lo que hablo, [míralo](https://www.digitalocean.com/community/tutorials/how-to-create-a-self-signed-ssl-certificate-for-apache-on-centos-8)  

```
cd /etc/httpd/conf.d/
nano mozillaSync.conf
```

Para http usa este: 
```
<VirtualHost *:80>
  ServerName localhost
  DocumentRoot /opt/syncstorage-rs/
  ProxyPreserveHost On
  ProxyPass / http://127.0.0.1:8000/
  ProxyPassReverse / http://127.0.0.1:8000/
  <Proxy *>
     AllowOverride all
     Require all granted
  </Proxy>
</VirtualHost>
```

o para https, usa ese:
```
<VirtualHost *:443>
   SSLEngine on
   SSLCertificateFile /path/to.crt
   SSLCertificateKeyFile /path/to.key
   ServerName myserver.tld
   DocumentRoot /opt/syncstorage-rs/
   RequestHeader set X-Forwarded-Proto https
   ProxyPreserveHost On
   ProxyPass / http://127.0.0.1:8000/
   ProxyPassReverse / http://127.0.0.1:8000/
   <Proxy *>
      AllowOverride all
      Require all granted
   </Proxy>
</VirtualHost>
```
Reinicia httpd

```
systemctl restart httpd
```

## Configuración en Firefox
Abre firefox y navega a "about:config". En el buscador pon ```tokenserver.uri```. Aquí debes insertar lo siguiente:
```
http://yourip/1.0/sync/1.5
https://domain.tlz/1.0/sync/1.5
```
Guarda y sal de firefox.

Ahora puedes iniciar sesión o crear una cuenta y, por fin, sincronizar.


## Autor y reconocimientos
El autor de este manual es [agile-penguin](agile-penguin.com).
Pero esto solamente fue posible con la ayuda de la comunidad, especialmente de Jewelux.

## Licencia
Como se ha mencionado antes, esto no es de MOZILLA, por lo que no hay ninguna licencia. Con respecto al contenido de MOZILLA, sí que hay una licencia. Está [aquí](https://github.com/mozilla-services/syncstorage-rs/blob/master/LICENSE).
